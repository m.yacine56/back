const express = require("express");
const router = express.Router();
//importing controllers
const locataireController = require("../controllers/locatairesController");
//router.get("/test", locataireController.test);

// router.get("/save", (req, res) => {
//   res.json({ status: "Employeed Saved" });
// });

//router.get("/list", locataireController.list);
router.get(
  "/listConsultation/:id_cnt/:cod_log",
  locataireController.listConsultation
);
router.get("/review/:mount", locataireController.listConsultation);

module.exports = router;
