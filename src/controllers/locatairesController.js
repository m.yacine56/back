const controllers = {};

var Consultation = require("../model/consultation");
var sequelize = require("../model/database");
const { QueryTypes } = require("sequelize");

sequelize.sync();

// controllers.testdata = async (req, res) => {
//   const response = await sequelize
//     .sync()
//     .then(function () {
//       const data = Employee.findAll();
//       return data;
//     })
//     .catch((err) => {
//       return err;
//     });
//   res.json(response);
// };
// controllers.list = async (req, res) => {
//   const data = await Employee.findAll({
//     include: [Role],
//   })
//     .then(function (data) {
//       return data;
//     })
//     .catch((error) => {
//       return error;
//     });

//   res.json({ success: true, data: data });
// };
controllers.listConsultation = async (req, res) => {
  const data = await Contrat.findAll();
  res.json(data);
};

controllers.listConsultation = async (req, res) => {
  var id_cnt = req.params.id_cnt;
  var cod_log = req.params.cod_log;

  const data = await sequelize.query(
    "select consultation.id_cnt,contrat.id_att,logement.cod_log, occupant.nom_oc , occupant.prn_oc , occupant.dnais_oc,occupant.nom_oc_ar , occupant.prn_oc_ar, cite.lib_cit ,type_construction.lib_cnst,batiment.lib_bat,logement.num_log,logement.escl_log,logement.SURF_HAB_LOG,a_usage.LIB_USG,contrat.DAT_CNT_DU,contrat.DAT_CNT_AU,consultation.DER_MP,consultation.loyer,consultation.charge,consultation.mens,ROUND(DATEDIFF(CURRENT_DATE, STR_TO_DATE(consultation.DER_MP,'%d/%m/%Y'))/30) as nbRetard from consultation,contrat,logement,occupant,batiment,cite,type_construction,a_usage where consultation.id_cnt like contrat.id_cnt and contrat.id_log like logement.id_log and consultation.id_oc like occupant.id_oc and logement.id_bat like batiment.id_bat and  batiment.id_cit like cite.id_cit  and logement.id_cnst like type_construction.id_cnst and contrat.id_usg like a_usage.id_usg and contrat.id_cnt = ?  and logement.cod_log = ?  ",
    {
      replacements: [id_cnt, cod_log],
      type: QueryTypes.SELECT,
    }
  );
  res.json({ success: true, data: data });
};
//Create role

module.exports = controllers;
