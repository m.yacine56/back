//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID

const Logement = sequelize.define(
  "Logement",
  {
    ID_LOG: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    ID_BAT: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    ID_CNST: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    COD_LOG: {
      type: Sequelize.STRING,
    },
    NUM_LOG: {
      type: Sequelize.INTEGER,
    },
    POS_LOG: {
      type: Sequelize.STRING,
    },
    NUM_ETG_LOG: {
      type: Sequelize.INTEGER,
    },
    ESCL_LOG: {
      type: Sequelize.INTEGER,
    },
    NAT_LOG: {
      type: Sequelize.STRING,
    },
    TYP_LOG: {
      type: Sequelize.STRING,
    },
    DAT_EXP_LOG: {
      type: Sequelize.STRING,
    },
    PRIX_REV_LOG: {
      type: Sequelize.STRING,
    },
    SURF_HAB_LOG: {
      type: Sequelize.STRING,
    },
    SURF_UTL_LOG: {
      type: Sequelize.STRING,
    },
    MIS_A_PRIX: {
      type: Sequelize.STRING,
    },
    UNIT: {
      type: Sequelize.INTEGER,
    },
    DAT: {
      type: Sequelize.STRING,
    },
    TYP_OP: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "logement", timestamps: false, underscored: false }
);

module.exports = Logement;
