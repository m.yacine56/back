//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID

const Batiment = sequelize.define(
  "Batiment",
  {
    id_bat: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    id_cit: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    id_rcp: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    lib_bat: {
      type: Sequelize.STRING,
    },
    nb_f1: {
      type: Sequelize.INTEGER,
    },
    nb_f2: {
      type: Sequelize.INTEGER,
    },
    nb_f3: {
      type: Sequelize.INTEGER,
    },
    nb_f4: {
      type: Sequelize.INTEGER,
    },
    nb_f5: {
      type: Sequelize.INTEGER,
    },
    nb_f6: {
      type: Sequelize.INTEGER,
    },
    nb_f7: {
      type: Sequelize.INTEGER,
    },
    nb_f8: {
      type: Sequelize.INTEGER,
    },
    nb_f9: {
      type: Sequelize.INTEGER,
    },
    nb_loc: {
      type: Sequelize.INTEGER,
    },
    nb_etag: {
      type: Sequelize.INTEGER,
    },
    surf_bat: {
      type: Sequelize.FLOAT,
    },
    nb_cage: {
      type: Sequelize.INTEGER,
    },
    tot_bat: {
      type: Sequelize.STRING,
    },
    cav_bat: {
      type: Sequelize.STRING,
    },
    orig_bn: {
      type: Sequelize.STRING,
    },
    unit: {
      type: Sequelize.INTEGER,
    },
    dat: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "batiment", timestamps: false, underscored: false }
);

module.exports = Batiment;
