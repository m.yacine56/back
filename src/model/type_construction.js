//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");

const Construction = sequelize.define(
  "Construction",
  {
    id_cnst: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    lib_cnst: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
    dat: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "type_construction", timestamps: false, underscored: false }
);

module.exports = Construction;
