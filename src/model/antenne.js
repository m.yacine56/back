//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID

const Antenne = sequelize.define(
  "Antenne",
  {
    id_ant: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    id_unit: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    lib_ant: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    lib_ant_ar: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
    dat: {
      type: Sequelize.STRING,
    },
    id_com: {
      type: Sequelize.INTEGER,
    },
    adr_ant_fr: {
      type: Sequelize.STRING,
    },
    adr_ant_ar: {
      type: Sequelize.STRING,
    },
    bat: {
      type: Sequelize.STRING,
    },
    porte: {
      type: Sequelize.STRING,
    },
    PRIX_REV_LOG: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "antenne", timestamps: false, underscored: false }
);

module.exports = Antenne;
