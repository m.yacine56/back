//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID
var Contrat = require("./contrat");
var Logement = require("./logement");
var Occupant = require("./occupant");

const Consultation = sequelize.define(
  "Consultation",
  {
    ID_CNT: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: {
        model: Contrat,
        key: "ID_CNT",
      },
    },
    ID_LOG: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: {
        model: Logement,
        key: "id_log",
      },
    },

    ID_OC: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: {
        model: Occupant,
        key: "id_oc",
      },
    },
    LS: {
      type: Sequelize.INTEGER,
    },
    dat_occ: {
      type: Sequelize.STRING,
    },
    dat_att: {
      type: Sequelize.INTEGER,
    },
    loyer: {
      type: Sequelize.FLOAT,
    },
    charge: {
      type: Sequelize.FLOAT,
    },
    mnt_tva: {
      type: Sequelize.FLOAT,
    },
    mens: {
      type: Sequelize.FLOAT,
    },
    der_mp: {
      type: Sequelize.STRING,
    },
    mois_ar: {
      type: Sequelize.FLOAT,
    },
    arriere: {
      type: Sequelize.FLOAT,
    },
    unit: {
      type: Sequelize.INTEGER,
    },
    dat: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "consultation", timestamps: false, underscored: false }
);

Consultation.associate = function (models) {
  Consultation.belongsTo(models.Contrat, {
    foreignKey: "ID_CNT",
    as: "Contrat",
  });
};
//Consultation.HasOne(Contrat);
//Consultation.belongsTo(Logement);
//Consultation.belongsTo(Occupant);

module.exports = Consultation;
