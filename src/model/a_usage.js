//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");

const Usage = sequelize.define(
  "Usage",
  {
    id_usg: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    lib_usg: {
      type: Sequelize.STRING,
    },
    lib_usg_ar: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
    dat: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "a_usage", timestamps: false, underscored: false }
);

module.exports = Usage;
