//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID

const Contrat = sequelize.define(
  "Contrat",
  {
    ID_CNT: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    ID_USG: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },

    ID_OC: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    ID_ACT: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    ID_ATT: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    ID_LOG: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: {
        model: (logement = require("./Logement")),
        key: "id_log",
      },
    },
    NUM_CNT: {
      type: Sequelize.INTEGER,
    },
    ABAT_CNT: {
      type: Sequelize.STRING,
    },
    DAT_CNT_DU: {
      type: Sequelize.STRING,
    },
    DAT_CNT_AU: {
      type: Sequelize.STRING,
    },
    CAUT_CNT: {
      type: Sequelize.STRING,
    },
    DAT_RSL_CNT: {
      type: Sequelize.STRING,
    },
    OBS_CNT: {
      type: Sequelize.STRING,
    },
    MOT_DES: {
      type: Sequelize.STRING,
    },
    ID_OC_DES_PER: {
      type: Sequelize.STRING,
    },
    CO_LOC: {
      type: Sequelize.STRING,
    },
    DAT_ABA: {
      type: Sequelize.STRING,
    },
    UNIT: {
      type: Sequelize.INTEGER,
    },
    DAT: {
      type: Sequelize.STRING,
    },
    TYP_OP: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "contrat", timestamps: false, underscored: false }
);

//Employee.belongsTo(Role);

module.exports = Contrat;
