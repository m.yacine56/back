//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID

const Occupant = sequelize.define(
  "Occupant",
  {
    id_oc: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    oc_id_oc: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    nom_oc: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    prn_oc: {
      type: Sequelize.STRING,
    },
    dnais_oc: {
      type: Sequelize.STRING,
    },
    lnais_oc: {
      type: Sequelize.STRING,
    },
    nis_oc: {
      type: Sequelize.STRING,
    },
    sf_oc: {
      type: Sequelize.STRING,
    },
    nb_cjt: {
      type: Sequelize.INTEGER,
    },
    enf_oc: {
      type: Sequelize.INTEGER,
    },
    prn_per_oc: {
      type: Sequelize.STRING,
    },
    nom_mer_oc: {
      type: Sequelize.STRING,
    },
    prn_mer_oc: {
      type: Sequelize.STRING,
    },
    rev_oc: {
      type: Sequelize.FLOAT,
    },
    nom_oc_ar: {
      type: Sequelize.STRING,
    },
    prn_oc_ar: {
      type: Sequelize.STRING,
    },
    prn_per_oc_ar: {
      type: Sequelize.STRING,
    },
    nom_mer_oc_ar: {
      type: Sequelize.STRING,
    },
    prn_mer_oc_ar: {
      type: Sequelize.STRING,
    },
    adr_oc: {
      type: Sequelize.STRING,
    },
    tel_oc: {
      type: Sequelize.STRING,
    },
    obsv: {
      type: Sequelize.STRING,
    },
    cat: {
      type: Sequelize.STRING,
    },
    unit: {
      type: Sequelize.INTEGER,
    },
    dat: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
    sexe: {
      type: Sequelize.STRING,
    },
    id_fich: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
  },
  { tableName: "occupant", timestamps: false, underscored: false }
);

module.exports = Occupant;
