//import sequelize
var Sequelize = require("sequelize");
// importing connection database
var sequelize = require("./database");
// import model for FK roleID

const Cite = sequelize.define(
  "Cite",
  {
    id_cit: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    id_ant: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    id_lcl: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    lib_cit: {
      type: Sequelize.STRING,
    },
    lib_cit_ar: {
      type: Sequelize.STRING,
    },
    lib_anc: {
      type: Sequelize.STRING,
    },
    nb_log: {
      type: Sequelize.INTEGER,
    },
    nb_loc: {
      type: Sequelize.INTEGER,
    },
    vlr_loc: {
      type: Sequelize.FLOAT,
    },
    cof_zne: {
      type: Sequelize.FLOAT,
    },
    vlr_log: {
      type: Sequelize.FLOAT,
    },
    dat: {
      type: Sequelize.STRING,
    },
    typ_op: {
      type: Sequelize.STRING,
    },
  },
  { tableName: "cite", timestamps: false, underscored: false }
);

module.exports = Cite;
